package form_aendern;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.DropMode;

public class Form_aendern extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel lblTxtVer�ndern = new JLabel("Dieser Text soll ver\u00E4ndert werden");
	private JPanel panel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Form_aendern frame = new Form_aendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Form_aendern() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 353, 604);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		panel.setBounds(0, 0, 337, 565);
		contentPane.add(panel);
		panel.setLayout(null);
		
		
		lblTxtVer�ndern.setHorizontalAlignment(SwingConstants.CENTER);
		lblTxtVer�ndern.setBounds(10, 11, 317, 40);
		panel.add(lblTxtVer�ndern);
		
		JLabel lblAufgabe1 = new JLabel("Aufgabe 1: Hintergrund \u00E4ndern");
		lblAufgabe1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabe1.setBounds(10, 62, 207, 14);
		panel.add(lblAufgabe1);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnRot_clicked();
			}
		});
		btnRot.setBounds(10, 87, 89, 23);
		panel.add(btnRot);
		
		JButton btnGruen = new JButton("Gr\u00FCn");
		btnGruen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnGruen_clicked();
			}
		});
		btnGruen.setBounds(109, 87, 108, 23);
		panel.add(btnGruen);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnBlau_clicked();
			}
		});
		btnBlau.setBounds(221, 87, 108, 23);
		panel.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnGelb_clicked();
			}
		});
		btnGelb.setBounds(10, 121, 89, 23);
		panel.add(btnGelb);
		
		JButton btnStandard = new JButton("Standardfarbe");
		btnStandard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnStandard_clicked();
			}
		});
		btnStandard.setBounds(109, 121, 108, 23);
		panel.add(btnStandard);
		
		JButton btnFarbeWaehlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWaehlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnFarbeWaehlen_clicked();
			}
		});
		btnFarbeWaehlen.setBounds(221, 121, 108, 23);
		panel.add(btnFarbeWaehlen);
		
		JLabel lblAufgabe2 = new JLabel("Aufgabe 2: Text Formatieren");
		lblAufgabe2.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabe2.setBounds(10, 155, 207, 14);
		panel.add(lblAufgabe2);
		
		JButton btnArial = new JButton("Arial");
		btnArial.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnArial_clickes();
			}
		});
		btnArial.setBounds(10, 180, 89, 23);
		panel.add(btnArial);
		
		JButton btnComicSans = new JButton("Comic Sans MS");
		btnComicSans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnComicSans_clicked();
			}
		});
		btnComicSans.setBounds(109, 180, 108, 23);
		panel.add(btnComicSans);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnCourierNew_clicked();
			}
		});
		btnCourierNew.setBounds(221, 180, 108, 23);
		panel.add(btnCourierNew);
		
		textField = new JTextField("Hier etwas eingeben");
		textField.setBounds(10, 208, 317, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JButton btnLoeschen = new JButton("L\u00F6schen");
		btnLoeschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnLoeschen_clicked();
			}
		});
		btnLoeschen.setBounds(172, 239, 155, 23);
		panel.add(btnLoeschen);
		
		JButton btnEinfuegen = new JButton("Einf\u00FCgen");
		btnEinfuegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnEinfuegen_clicked();
			}
		});
		btnEinfuegen.setBounds(10, 239, 155, 23);
		panel.add(btnEinfuegen);
		
		JLabel lblAufgabe3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabe3.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabe3.setBounds(10, 273, 207, 14);
		panel.add(lblAufgabe3);
		
		JButton btnSRot = new JButton("Rot");
		btnSRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnSRot.setBounds(10, 298, 89, 23);
		panel.add(btnSRot);
		
		JButton btnSBlau = new JButton("Blau");
		btnSBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnSBlau.setBounds(109, 298, 108, 23);
		panel.add(btnSBlau);
		
		JButton btnSSchwarz = new JButton("Schwarz");
		btnSSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnSSchwarz.setBounds(221, 298, 106, 23);
		panel.add(btnSSchwarz);
		
		JLabel lblAufgabe4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabe4.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabe4.setBounds(10, 332, 207, 14);
		panel.add(lblAufgabe4);
		
		JButton btnGroesser = new JButton("+");
		btnGroesser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnGroesser_clicked();
			}
		});
		btnGroesser.setBounds(10, 357, 155, 23);
		panel.add(btnGroesser);
		
		JButton btnKleiner = new JButton("-");
		btnKleiner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnKleiner_clicked();
			}
		});
		btnKleiner.setBounds(172, 357, 155, 23);
		panel.add(btnKleiner);
		
		JLabel lblAufgabe5 = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabe5.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabe5.setBounds(10, 391, 207, 14);
		panel.add(lblAufgabe5);
		
		JButton btnLinks = new JButton("linksb\u00FCndig");
		btnLinks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnLinks_clicked();
			}
		});
		btnLinks.setBounds(10, 416, 89, 23);
		panel.add(btnLinks);
		
		JButton btnMitte = new JButton("zentriert");
		btnMitte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnMitte_clicked();
			}
		});
		btnMitte.setBounds(109, 416, 108, 23);
		panel.add(btnMitte);
		
		JLabel lblAufgabe6 = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabe6.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabe6.setBounds(10, 450, 207, 14);
		panel.add(lblAufgabe6);
		
		JButton btnBeenden = new JButton("EXIT");
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(EXIT_ON_CLOSE);
			}
		});
		btnBeenden.setBounds(10, 475, 317, 79);
		panel.add(btnBeenden);
		
		JButton btnRechts = new JButton("rechtsb\u00FCndig");
		btnRechts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnRechts_clicked();
			}
		});
		btnRechts.setBounds(221, 416, 108, 23);
		panel.add(btnRechts);
	}

	public void btnLoeschen_clicked() {
		lblTxtVer�ndern.setText("");		
	}

	public void btnEinfuegen_clicked() {
		lblTxtVer�ndern.setText(textField.getText());
	}

	public void btnCourierNew_clicked() {
		lblTxtVer�ndern.setFont(new Font("Courier New", Font.PLAIN, lblTxtVer�ndern.getFont().getSize()));		
	}

	public void btnComicSans_clicked() {
		lblTxtVer�ndern.setFont(new Font("Comic Sans MS", Font.PLAIN, lblTxtVer�ndern.getFont().getSize()));		
	}

	public void btnArial_clickes() {
		lblTxtVer�ndern.setFont(new Font("Arial", Font.PLAIN, lblTxtVer�ndern.getFont().getSize()));		
	}

	public void btnFarbeWaehlen_clicked() {
		JColorChooser cc = new JColorChooser();
		Color fw = cc.showDialog(null, "Farbe w�hlen", null);
		panel.setBackground(fw);
	}

	public void btnStandard_clicked() {
		panel.setBackground(new Color(0xEEEEEE));
	}

	public void btnGruen_clicked() {
		panel.setBackground(Color.GREEN);		
	}
	
	public void btnBlau_clicked() {
		panel.setBackground(Color.BLUE);
	}
	
	public void btnGelb_clicked() {
		panel.setBackground(Color.YELLOW);
	}

	public void btnRot_clicked() {
		panel.setBackground(Color.RED);
	}

	public void btnGroesser_clicked() {
		int groesse = lblTxtVer�ndern.getFont().getSize();
		lblTxtVer�ndern.setFont(new Font(lblTxtVer�ndern.getFont().getFontName(), Font.PLAIN, groesse + 1));
	}
	
	public void btnKleiner_clicked() {
		int groesse = lblTxtVer�ndern.getFont().getSize();
		lblTxtVer�ndern.setFont(new Font(lblTxtVer�ndern.getFont().getFontName(), Font.PLAIN, groesse - 1));
	}

	public void btnRechts_clicked() {
		lblTxtVer�ndern.setHorizontalAlignment(JLabel.RIGHT);		
	}

	public void btnMitte_clicked() {
		lblTxtVer�ndern.setHorizontalAlignment(JLabel.CENTER);		
	}

	public void btnLinks_clicked() {
		lblTxtVer�ndern.setHorizontalAlignment(JLabel.LEFT);
	}
}
